package jaaaaga.matchhandi;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import org.json.JSONObject;

import java.util.Calendar;


public class CreateMatchActivity extends AppCompatActivity {
    private SQLiteHandler db;
    private SessionManager session;
    private JSONManager jm;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog, endtimePickerDialog;
    private int mYear, mMonth, mDay, mHour, mMinute;

    @BindView(R.id.btn_creatematchrecord)
    Button createMatchRecord;
    @BindView(R.id.input_match_name)
    TextInputEditText inputMatchName;
    @BindView(R.id.creatematch_input_layout_match_name)
    TextInputLayout inputLayoutMatchName;
    @BindView(R.id.input_location)
    TextInputEditText inputLocation;
    @BindView(R.id.creatematch_input_layout_location)
    TextInputLayout inputLayoutLocation;
    @BindView(R.id.input_player1)
    TextInputEditText inputPlayer1;
    @BindView(R.id.creatematch_input_layout_player1)
    TextInputLayout inputLayoutPlayer1;
    @BindView(R.id.input_player2)
    TextInputEditText inputPlayer2;
    @BindView(R.id.creatematch_input_layout_player2)
    TextInputLayout inputLayoutPlayer2;
    @BindView(R.id.btn_pickstartdate)
    Button btn_pickstartdate;
    @BindView(R.id.btn_pickstarttime)
    Button btn_pickstarttime;
    @BindView(R.id.btn_pickendtime)
    Button btn_pickendtime;
    @BindView(R.id.input_startdate)
    TextInputEditText input_startdate;
    @BindView(R.id.creatematch_input_layout_startdate)
    TextInputLayout inputLayoutStartdate;
    @BindView(R.id.input_starttime)
    TextInputEditText input_starttime;
    @BindView(R.id.creatematch_input_layout_starttime)
    TextInputLayout inputLayoutStart;
    @BindView(R.id.input_endtime)
    TextInputEditText input_endtime;
    @BindView(R.id.creatematch_input_layout_endtime)
    TextInputLayout inputLayoutEnd;
    @BindView(R.id.spinner)
    Spinner spinner;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creatematch);
        ButterKnife.bind(this);

        // SqLite database handler
        db = new SQLiteHandler(CreateMatchActivity.this);

        // session manager
        session = new SessionManager(CreateMatchActivity.this);

        jm = new JSONManager(CreateMatchActivity.this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        String Startdate = null;

        datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener(){
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String month;
                if ((monthOfYear + 1) < 10)  month = "0" + (monthOfYear+1);
                else month = String.valueOf(monthOfYear +1);
                String day;
                if (dayOfMonth < 10) day = "0" + dayOfMonth;
                else day = String.valueOf(dayOfMonth);
                input_startdate.setText(year + "-" + month + "-" + day);
            }
        }, mYear,mMonth,mDay);

        btn_pickstartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

        timePickerDialog = new TimePickerDialog(this, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener(){
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String hour;
                if (hourOfDay < 10) hour = "0"+ hourOfDay;
                else hour = String.valueOf(hourOfDay);
                String min;
                if (minute < 10) min = "0" + minute;
                else min = String.valueOf(minute);
                        input_starttime.setText(hour + ":" + min);
            }
        }, mHour, mMinute, false);

        btn_pickstarttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog.show();
            }
        });

        endtimePickerDialog = new TimePickerDialog(this, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener(){
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String hour;
                if (hourOfDay < 10) hour = "0"+ hourOfDay;
                else hour = String.valueOf(hourOfDay);
                String min;
                if (minute < 10) min = "0" + minute;
                else min = String.valueOf(minute);
                input_endtime.setText(hour + ":" + min);
            }
        }, mHour, mMinute, false);

        btn_pickendtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endtimePickerDialog.show();
            }
        });


        final String[] status_options = {"Scheduled", "Started", "Finished"};
        ArrayAdapter<String> status_list = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, status_options);
        spinner.setAdapter(status_list);

        createMatchRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) createMatch();
            }
        });



    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void createMatch() {
                String name = inputMatchName.getText().toString();

                String type = inputLocation.getText().toString();

                String start = input_startdate.getText().toString() + " " + input_starttime.getText().toString() + ":00";

                String end = input_startdate.getText().toString() + " " + input_endtime.getText().toString() + ":00";

                int status = 0;

                switch (spinner.getSelectedItem().toString()) {
                    case "Started":
                        status = 1;
                        break;
                    case "Finished":
                        status = 2;
                        break;
                    default:
                }



                final String p1 = inputPlayer1.getText().toString();

                final String p2 = inputPlayer2.getText().toString();



        jm.db_newMatch(new Match(-1, name, type, status, start, end), p1, p2, new JSONManager.ServerCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                JSONManager jm = new JSONManager(getApplicationContext());
                jm.db_newScore(session.getCurrentMatch(), p1, 0, p2, 0, new JSONManager.ServerCallback() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        Intent MatchIntent = new Intent(getApplicationContext(), MatchActivity.class);
//                        MatchIntent.putExtra("matchID", session.getNewCreatedMatch());
                        startActivity(MatchIntent);
                        finish();
                    }
                });
            }
        });

    }

    private boolean validate() {
        boolean valid = true;

        String matchname = inputMatchName.getText().toString();
        String location = inputLocation.getText().toString();
        String date = input_startdate.getText().toString();
        String start = input_starttime.getText().toString();
        String end = input_endtime.getText().toString();
        String p1_name = inputPlayer1.getText().toString();
        String p2_name = inputPlayer2.getText().toString();

        if (matchname.isEmpty() || matchname.length() > 50) {
            inputLayoutMatchName.setError("Please enter a proper name");
            valid = false;
        } else {
            inputLayoutMatchName.setError(null);
        }

        if (location.isEmpty()) {
            inputLayoutLocation.setError("Please enter a location");
            valid = false;
        } else {
            inputLayoutLocation.setError(null);
        }

        if (date.isEmpty()) {
            inputLayoutStartdate.setError("Please enter a date");
            valid = false;
        } else {
            inputLayoutStartdate.setError(null);
        }


        if (start.compareTo(end) >= 0) {
            inputLayoutStart.setError("Please enter a proper time");
            inputLayoutEnd.setError("Please enter a proper time");
            valid = false;
        } else {
            inputLayoutStart.setError(null);
            inputLayoutEnd.setError(null);
        }

        if (p1_name.isEmpty() || p1_name.length() > 25) {
            inputLayoutPlayer1.setError("Please enter a proper Name");
            valid = false;
        } else {
            inputLayoutPlayer1.setError(null);
        }

        if (p2_name.isEmpty() || p2_name.length() > 25) {
            inputLayoutPlayer2.setError("Password does not match");
            valid = false;
        } else {
            inputLayoutPlayer2.setError(null);
        }

        return valid;
    }
}
