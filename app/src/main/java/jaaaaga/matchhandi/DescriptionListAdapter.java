package jaaaaga.matchhandi;

import android.app.Activity;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class DescriptionListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] description_items;
    private final String[] description_details;
    private final Integer[] description_icons;

    public DescriptionListAdapter(Activity context, String[] description_items, String[] description_details, Integer[] description_icons) {
        super(context, R.layout.content_match_description, description_items);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.description_icons =description_icons;
        this.description_items=description_items;
        this.description_details=description_details;
    }

    public View getView(int position,View view,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.content_match_description, null,true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.description_detail);
        TextView extratxt = (TextView) rowView.findViewById(R.id.description_title);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.description_icon);

        txtTitle.setText(description_details[position]);
        extratxt.setText(description_items[position]);
        imageView.setImageResource(description_icons[position]);


        return rowView;

    };
}
