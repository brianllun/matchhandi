package jaaaaga.matchhandi;


import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.android.volley.VolleyLog.TAG;

public class JSONManager {
    private SessionManager session;
    private SQLiteHandler db;
    private Context context;
    private ProgressDialog pDialog;

    JSONManager(Context context) {
        this.context = context;
        pDialog = new ProgressDialog(context);
        db = new SQLiteHandler(context);
        session = new SessionManager(context);
        db_getMatch(new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result) {
            }
        });
//        db_getScore();
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void db_newMatch(final Match m, final String p1, final String p2, final ServerCallback callback){
        String tag_string_req = "req_savematch";

        pDialog.setMessage("Adding Match...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SAVEMATCH, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "NEWMATCH Response: " + response);
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {

                        String match_id = jObj.getString("match_id");
                        JSONObject jArr = jObj.getJSONObject("match");

                        String owner_id = jArr.getString("owner_id");
                        String board_id = jArr.getString("board_id");
                        String name = jArr.getString("name");
                        String type = jArr.getString("type");
                        String status = jArr.getString("status");
                        String created_at = jArr.getString("created_at");
                        String start_at = jArr.getString("start_at");
                        String finish_at = jArr.getString("finish_at");

                        db.addMatch(match_id, owner_id, board_id, name,
                                type, status, created_at, start_at, finish_at);

                        session.setCurrentMatch(jObj.getInt("match_id"));
                        callback.onSuccess(jObj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "NEWMATCH Error: " + error.getMessage());
                Toast.makeText(context,
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<>();
                params.put("match_id", String.valueOf(m.getMatchID()));
                params.put("owner_id", String.valueOf(m.getOwnerID()));
                params.put("board_id", String.valueOf(m.getBoardID()));
                params.put("name", m.getName());
                params.put("type", m.getType());
                params.put("status", String.valueOf(m.getStatus()));
                params.put("start_at", m.getStart_at());
                params.put("finish_at", m.getFinish_at());
                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    public void db_getMatch(final ServerCallback callback) {
        // Tag used to cancel the request
        String tag_string_req = "req_match";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETMATCH, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Fetch Match Response: " + response);
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");


                    // Check for error node in json
                    if (!error) {

                        db.resetMatches();
                        JSONArray match = jObj.getJSONArray("match");

                        JSONArray jArr = new JSONArray(match.toString().trim());
//                        Iterator<?> keys = jObject.keys();

                        Log.d(TAG, "Array size: " + jArr.length());
                        for (int i = 0; i < jArr.length(); i++) {
                            String match_id = jArr.getJSONObject(i).getString("match_ID");
                            String owner_id = jArr.getJSONObject(i).getString("owner_ID");
                            String board_id = jArr.getJSONObject(i).getString("board_ID");
                            String name = jArr.getJSONObject(i).getString("name");
                            String type = jArr.getJSONObject(i).getString("type");
                            String status = jArr.getJSONObject(i).getString("status");
                            String created_at = jArr.getJSONObject(i).getString("created_at");
                            String start_at = jArr.getJSONObject(i).getString("start_at");
                            String finish_at = jArr.getJSONObject(i).getString("finish_at");

                            Log.d(TAG, match_id);
                            db.addMatch(match_id, owner_id, board_id, name, type, status, created_at, start_at, finish_at);
                            callback.onSuccess(jObj);
                        }
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(context, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(context,
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("match_id", "-1");
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void db_newScore(final int matchid, final String p1_n, final int p1, final String p2_n, final int p2, final ServerCallback callback){
        String tag_string_req = "req_newscore";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_WRITESCORE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Score Response: " + response);
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {

                        String match_id = jObj.getString("match_id");
                        JSONObject jArr = jObj.getJSONObject("score");

                        String score_id = jArr.getString("score_id");
                        String p1_name = jArr.getString("p1_name");
                        String p1_score = jArr.getString("p1_score");
                        String p2_name = jArr.getString("p2_name");
                        String p2_score = jArr.getString("p2_score");
                        String created_at = jArr.getString("created_at");

                        db.addScore(score_id, match_id, p1_name, p1_score, p2_name, p2_score, created_at);

                        session.setCurrentScore(jArr.getInt("score_id"));
                        callback.onSuccess(jObj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Save Score Error: " + error.getMessage());
                Toast.makeText(context,
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<>();
                params.put("score_id", "-1");
                params.put("match_id", String.valueOf(matchid));
                params.put("p1_name", p1_n);
                params.put("p1_score", String.valueOf(p1));
                params.put("p2_name", p2_n);
                params.put("p2_score", String.valueOf(p2));
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    public void db_getScore(final ServerCallback callback) {
        // Tag used to cancel the request
        String tag_string_req = "req_score";
        final String match_id = String.valueOf(session.getCurrentMatch());
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETSCORE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Fetch Score Response: " + response);
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");


                    // Check for error node in json
                    if (!error) {
                        JSONObject score = jObj.getJSONObject("score");
                        String score_id = score.getString("score_id");
                        String p1_name = score.getString("p1_name");
                        String p1_score = score.getString("p1_score");
                        String p2_name = score.getString("p2_name");
                        String p2_score = score.getString("p2_score");
                        String created_at = score.getString("created_at");

                        db.resetScores();
                        db.addScore(score_id, match_id, p1_name, p1_score, p2_name, p2_score, created_at);
                        callback.onSuccess(jObj);
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(context, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(context,
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("match_id", match_id);
                Log.d(TAG, "currmatch" + session.getCurrentMatch());
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void db_updateScore(final ServerCallback callback) {
        // Tag used to cancel the request
        String tag_string_req = "update_score";
        final String match_id = String.valueOf(session.getCurrentMatch());
        final String created_at = String.valueOf(db.getScore().get(session.getCurrentMatch()).getCreated_at());
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATESCORE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Update Score Response: " + response);
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");


                    // Check for error node in json
                    if (!error) {

                        String match_id = jObj.getString("match_id");

                        if (!match_id.equals("-1")) {

                            JSONObject score = jObj.getJSONObject("score");
                            String score_id = score.getString("score_id");
                            String p1_name = score.getString("p1_name");
                            String p1_score = score.getString("p1_score");
                            String p2_name = score.getString("p2_name");
                            String p2_score = score.getString("p2_score");
                            String created_at = score.getString("created_at");
                            db.deleteScores();
                            db.addScore(score_id, match_id, p1_name, p1_score, p2_name, p2_score, created_at);
                        }

                        callback.onSuccess(jObj);
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(context, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(context,
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("match_id", match_id);
                params.put("update", created_at);
                Log.d(TAG, "currmatch" + session.getCurrentMatch());
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    public interface ServerCallback{
        void onSuccess(JSONObject result);
    }
}
