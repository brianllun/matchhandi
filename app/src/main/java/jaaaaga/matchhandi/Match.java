package jaaaaga.matchhandi;

import android.media.tv.TvInputService;
import android.os.Parcel;
import android.os.Parcelable;

public class Match implements Parcelable{
    private int matchID;
    private int ownerID;
    private int boardID;
    private String name;
    private String type;
    private int status;
    private String created_at;
    private String start_at;
    private String finish_at;
    private int resourceID;
    private SessionManager session = new SessionManager(MainActivity.getAppContext());

    Match(int matchID, int ownerID, int boardID,
          String name, String type, int status,
          String created_at, String start_at, String finish_at) {
        this.matchID = matchID;
        this.ownerID = ownerID;
        this.boardID = boardID;
        this.name = name;
        this.type = type;
        this.status = status;
        this.created_at = created_at;
        this.start_at = start_at;
        this.finish_at = finish_at;
    }
    Match() {
    }

    Match(int matchID, String name, String type, int status, String start_at, String finish_at) {
        this.matchID = matchID;
        this.ownerID = session.getCurrentUserID();
        this.boardID = 1;
        this.name = name;
        this.type = type;
        this.status = status;
        this.start_at = start_at;
        this.finish_at = finish_at;
    }

    Match(int matchID) {
        this.matchID = matchID;
    }

    public String getName() {
        return (this.name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMatchID() {
        return (this.matchID);
    }

    public int getResourceID() {
        return (this.resourceID);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(matchID);
        dest.writeInt(resourceID);
        dest.writeString(name);
    }

    public static final Parcelable.Creator<Match> CREATOR = new Creator<Match>() {
        public Match createFromParcel(Parcel source) {
            Match mMatch = new Match();
            mMatch.matchID = source.readInt();
            mMatch.resourceID = source.readInt();
            mMatch.name = source.readString();
            return mMatch;
        }
        public Match[] newArray(int size) {
            return new Match[size];
        }
    };

    public String getFinish_at() {
        return finish_at;
    }

    public void setFinish_at(String finish_at) {
        this.finish_at = finish_at;
    }

    public String getStart_at() {
        return start_at;
    }

    public void setStart_at(String start_at) {
        this.start_at = start_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getBoardID() {
        return boardID;
    }

    public void setBoardID(int boardID) {
        this.boardID = boardID;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }
}
