package jaaaaga.matchhandi;

import android.widget.Filter;
import java.util.ArrayList;
import java.util.List;

public class MatchFilter extends Filter{
    private RVAdapter adapter;
    ArrayList<Match> filterList;
    public MatchFilter(ArrayList<Match> filterList,RVAdapter adapter)
    {
        this.adapter=adapter;
        this.filterList=filterList;
    }
    //FILTERING OCCURS
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        //CHECK CONSTRAINT VALIDITY
        if(constraint != null && constraint.length() > 0)
        {
            //CHANGE TO UPPER
            constraint=constraint.toString().toUpperCase();
            //STORE OUR FILTERED PLAYERS
            ArrayList<Match> filteredMatches=new ArrayList<>();
            for (int i=0;i<filterList.size();i++)
            {
                //CHECK
                if(filterList.get(i).getName().toUpperCase().contains(constraint))
                {
                    //ADD PLAYER TO FILTERED PLAYERS
                    filteredMatches.add(filterList.get(i));
                }
            }
            results.count=filteredMatches.size();
            results.values=filteredMatches;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.matchList= (ArrayList<Match>) results.values;
        //REFRESH
        adapter.notifyDataSetChanged();
    }
}