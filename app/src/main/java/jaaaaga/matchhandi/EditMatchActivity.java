package jaaaaga.matchhandi;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import org.json.JSONObject;

import java.util.Calendar;


public class EditMatchActivity extends AppCompatActivity {

    @BindView(R.id.einput_matchname)
    TextInputEditText inputMatchName;
    @BindView(R.id.e_input_layout_matchname)
    TextInputLayout inputLayoutMatchName;
    @BindView(R.id.einput_location)
    TextInputEditText inputLocation;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog, endtimePickerDialog;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private String name, date, starttime, endtime, location, p1name, p2name;
    private int status;
    @BindView(R.id.e_input_layout_location)
    TextInputLayout inputLayoutLocation;
    @BindView(R.id.einput_player1)
    TextInputEditText inputPlayer1;
    @BindView(R.id.e_input_layout_player1)
    TextInputLayout inputLayoutPlayer1;
    @BindView(R.id.einput_player2)
    TextInputEditText inputPlayer2;
    @BindView(R.id.e_input_layout_player2)
    TextInputLayout inputLayoutPlayer2;
    @BindView(R.id.ebtn_pickstartdate)
    Button btn_pickstartdate;
    @BindView(R.id.ebtn_pickstarttime)
    Button btn_pickstarttime;
    @BindView(R.id.ebtn_pickendtime)
    Button btn_pickendtime;
    @BindView(R.id.einput_startdate)
    TextInputEditText input_startdate;
    @BindView(R.id.einput_layout_startdate)
    TextInputLayout inputLayoutStartdate;
    @BindView(R.id.einput_starttime)
    TextInputEditText input_starttime;
    @BindView(R.id.einput_layout_starttime)
    TextInputLayout inputLayoutStart;
    @BindView(R.id.einput_endtime)
    TextInputEditText input_endtime;
    @BindView(R.id.einput_layout_endtime)
    TextInputLayout inputLayoutEnd;
    @BindView(R.id.espinner)
    Spinner spinner;
    @BindView(R.id.ebtn_editmatch)
    Button editmatch;
    private SQLiteHandler db;
    private SessionManager session;
    private JSONManager jm;

    @OnClick(R.id.ebtn_editmatch)
    public void run() {
        if (validate()) updateMatch();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editmatch);
        ButterKnife.bind(this);
        Intent myintent = getIntent();
        name = myintent.getStringExtra("Name");
        location = myintent.getStringExtra("Location");
        date = myintent.getStringExtra("Date");
        starttime = myintent.getStringExtra("StartTime");
        endtime = myintent.getStringExtra("EndTime");
        status = myintent.getIntExtra("Status", 0);
        p1name = myintent.getStringExtra("P1Name");
        p2name = myintent.getStringExtra("P2Name");

        // SqLite database handler
        db = new SQLiteHandler(this);

        // session manager
        session = new SessionManager(this);

        jm = new JSONManager(this);

        inputMatchName.setText(name);
        inputLocation.setText(location);
        input_startdate.setText(date);
        input_starttime.setText(starttime);
        input_endtime.setText(endtime);
        inputPlayer1.setText(p1name);
        inputPlayer2.setText(p2name);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);



        datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener(){
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                input_startdate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
            }
        }, mYear,mMonth,mDay);

        btn_pickstartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

        timePickerDialog = new TimePickerDialog(this, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener(){
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String hour;
                if (hourOfDay < 10) hour = "0"+ hourOfDay;
                else hour = String.valueOf(hourOfDay);
                String min;
                if (minute < 10) min = "0" + minute;
                else min = String.valueOf(minute);
                input_starttime.setText(hour + ":" + min);
            }
        }, mHour, mMinute, false);

        btn_pickstarttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog.show();
            }
        });

        endtimePickerDialog = new TimePickerDialog(this, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener(){
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String hour;
                if (hourOfDay < 10) hour = "0"+ hourOfDay;
                else hour = String.valueOf(hourOfDay);
                String min;
                if (minute < 10) min = "0" + minute;
                else min = String.valueOf(minute);
                input_endtime.setText(hour + ":" + min);
            }
        }, mHour, mMinute, false);

        btn_pickendtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endtimePickerDialog.show();
            }
        });

        final String[] status_options = {"Scheduled", "Started", "Finished"};
        ArrayAdapter<String> status_list = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, status_options);
        spinner.setAdapter(status_list);
        spinner.setSelection(status);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private boolean validate() {
        boolean valid = true;

        String matchname = inputMatchName.getText().toString();
        String location = inputLocation.getText().toString();
        String date = input_startdate.getText().toString();
        String start = input_starttime.getText().toString();
        String end = input_endtime.getText().toString();
        String p1_name = inputPlayer1.getText().toString();
        String p2_name = inputPlayer2.getText().toString();

        if (matchname.isEmpty() || matchname.length() > 50) {
            inputLayoutMatchName.setError("Please enter a proper name");
            valid = false;
        } else {
            inputLayoutMatchName.setError(null);
        }

        if (location.isEmpty()) {
            inputLayoutLocation.setError("Please enter a location");
            valid = false;
        } else {
            inputLayoutLocation.setError(null);
        }

        if (date.isEmpty()) {
            inputLayoutStartdate.setError("Please enter a date");
            valid = false;
        } else {
            inputLayoutStartdate.setError(null);
        }


        if (start.compareTo(end) >= 0) {
            inputLayoutStart.setError("Please enter a proper time");
            inputLayoutEnd.setError("Please enter a proper time");
            valid = false;
        } else {
            inputLayoutStart.setError(null);
            inputLayoutEnd.setError(null);
        }

        if (p1_name.isEmpty() || p1_name.length() > 25) {
            inputLayoutPlayer1.setError("Please enter a proper Name");
            valid = false;
        } else {
            inputLayoutPlayer1.setError(null);
        }

        if (p2_name.isEmpty() || p2_name.length() > 25) {
            inputLayoutPlayer2.setError("Password does not match");
            valid = false;
        } else {
            inputLayoutPlayer2.setError(null);
        }

        return valid;
    }

    private void updateMatch() {
        String name = inputMatchName.getText().toString();
        String type = inputLocation.getText().toString();
        String start = input_startdate.getText().toString() + " " + input_starttime.getText().toString() + ":00";
        String end = input_startdate.getText().toString() + " " + input_endtime.getText().toString() + ":00";

        int status = 0;

        switch (spinner.getSelectedItem().toString()) {
            case "Started":
                status = 1;
                break;
            case "Finished":
                status = 2;
                break;
            default:
        }

        final String p1 = inputPlayer1.getText().toString();
        final String p2 = inputPlayer2.getText().toString();


        jm.db_newMatch(new Match(session.getCurrentMatch(), name, type, status, start, end), p1, p2, new JSONManager.ServerCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                Score s = db.getScore().get(session.getCurrentMatch());
                jm.db_newScore(session.getCurrentMatch(), p1, s.getP1_score(), p2, s.getP2_score(), new JSONManager.ServerCallback() {
                    @Override
                    public void onSuccess(JSONObject result) {
//                        MatchIntent.putExtra("matchID", session.getNewCreatedMatch());
                        jm.db_getMatch(new JSONManager.ServerCallback() {
                            @Override
                            public void onSuccess(JSONObject result) {
                                finish();
                            }
                        });
                    }
                });
            }
        });

    }
}
