package jaaaaga.matchhandi;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import org.json.JSONObject;
import android.support.v7.widget.SearchView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    @BindView(R.id.name)
    TextView txtName;
    @BindView(R.id.email)
    TextView txtEmail;
    @BindView(R.id.btn_creatematch)
    FloatingActionButton btnCreateMatch;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.appbar)
    AppBarLayout appBarLayout;
    @BindView(R.id.mSearch)
    SearchView searchMatch;
    @BindView(R.id.search_src_text)
    EditText searchEditText;
            //= (EditText) searchMatch.findViewById(android.support.v7.appcompat.R.id.search_src_text);

    private SQLiteHandler db;
    private SessionManager session;
    private JSONManager jm;
    private RVAdapter adapter;
    private ArrayList<Match> matchList;
    private Map<Integer, Match> matchMap;
    private Context c;
    public static Context context;
    private RVAdapter.RVAdapterListener listener = new RVAdapter.RVAdapterListener() {
        @Override
        public void onCardSelected(int pos) {
            searchEditText.setText("");
            searchMatch.onActionViewCollapsed();
            toMatchActivity(pos);
        }

        private void toMatchActivity(int pos) {
            int matchPos = pos;
            Intent MatchIntent = new Intent(MainActivity.this, MatchActivity.class);
//            MatchIntent.putExtra("matchID", matchList.get(pos).getMatchID());
            session.setCurrentMatch(RVAdapter.matchList.get(pos).getMatchID());
            startActivityForResult(MatchIntent, 1);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                DBRefresh();
                RVRefresh();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        c = this;
        this.context = this;
        db = new SQLiteHandler(c);
        session = new SessionManager(c);
        jm = new JSONManager(c);

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        setSupportActionBar(toolbar);
        initCollapsingToolbar();

        matchList = new ArrayList<>();
        adapter = new RVAdapter(c, matchList, listener);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(c, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {//設置監聽器
            @Override
            public void onRefresh() {
                // TODO: 28/11/2017  Add refresh logic
                jm.db_getMatch(new JSONManager.ServerCallback() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        RVRefresh();
                        swipeLayout.setRefreshing(false);
                    }
                });
            }
        });

        DBRefresh();

        searchMatch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                //FILTER AS YOU TYPE
                adapter.getFilter().filter(query);
                return false;
            }
        });

        searchEditText.setTextColor(getResources().getColor(R.color.primary_dark));
        searchEditText.setHintTextColor(getResources().getColor(R.color.aluminum));

        searchMatch.setOnClickListener(new SearchView.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchMatch.onActionViewExpanded();
            }
        });
    }
    public void DBRefresh() {
        jm.db_getMatch(new JSONManager.ServerCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                prepareMatches();
                // Fetching user details from sqlite
                HashMap<String, String> user = db.getUserDetails();

                String name = user.get("username");
                String email = user.get("email");

                // Displaying the user details on the screen
                txtName.setText(name);
                txtEmail.setText(email);
                Log.v(TAG, "usrname=" + name);
                // Create Match click event
                btnCreateMatch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent myIntent = new Intent(v.getContext(), CreateMatchActivity.class);
                        startActivity(myIntent);
                    }
                });
            }
        });
    }
    public void RVRefresh() {
        matchList.clear();
        matchMap = db.getMatch();
        for (Map.Entry<Integer, Match> match : matchMap.entrySet()) {
            matchList.add(match.getValue());
        }
        adapter.updateList(matchList);
        adapter.notifyDataSetChanged();
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void prepareMatches() {
        Log.d(TAG, "Match TO RV");
        matchList.clear();
        matchMap = db.getMatch();
        for (Map.Entry<Integer, Match> match : matchMap.entrySet()) {
            matchList.add(match.getValue());
        }

//        int[] covers = new int[]{
//                R.drawable.match1,
//                R.drawable.match2,
//        };

//        Match a = new Match("True Romance", 13, covers[0]);
//        matchList.add(a);
//

        adapter.notifyDataSetChanged();
    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle("MatchHandi");
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     */
    private void logoutUser() {
        session.setLogin(false);

        db.deleteUsers();
        db.deleteMatches();
        db.deleteScores();

        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_out: {
                logoutUser();
                break;
            }
            case R.id.action_refresh: {
                db.resetMatches();
                Intent intent = getIntent();
                finish();
                startActivity(intent);
                overridePendingTransition(0, 0);
                break;
            }
            // case blocks for other MenuItems (if any)
        }
        return false;
    }

    public static Context getAppContext() {
        return context;
    }
}