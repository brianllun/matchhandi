package jaaaaga.matchhandi;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class MatchActivity extends AppCompatActivity {

//    @BindView(R.id.match_name)
//    TextView txtName;

    private Match m;
    private Score s;
    private Map<String, String> u;
    private int matchid, scoreid;
    private String date, starttime, endtime, status;


    private SQLiteHandler db;
    private SessionManager session;
    private JSONManager jm;
    private Context c;
    private Handler uih = new Handler();
    private Handler sh;
    private HandlerThread st;

    @Nullable @BindView(R.id.btn_editmatch)
    FloatingActionButton btnEditMatch;
    @BindView(R.id.p1_score)
    TextView p1_score;
    @BindView(R.id.p1_name)
    TextView p1_name;
    @BindView(R.id.p2_score)
    TextView p2_score;
    @BindView(R.id.p2_name)
    TextView p2_name;
    @Nullable @BindView(R.id.btn_p1_increase_score)
    Button btn_p1_increase_score;
    @Nullable @BindView(R.id.btn_p1_decrease_score)
    Button btn_p1_decrease_score;
    @Nullable @BindView(R.id.btn_p2_increase_score)
    Button btn_p2_increase_score;
    @Nullable @BindView(R.id.btn_p2_decrease_score)
    Button btn_p2_decrease_score;
    @Nullable @BindView(R.id.marksPer)
    EditText marksPer;
    @Nullable @BindView(R.id.btn_add)
    Button btn_add;
    @Nullable @BindView(R.id.btn_minus)
    Button btn_minus;
    @Nullable @BindView(R.id.MarkDefinition)
    LinearLayout mkd;
    @Nullable @BindView(R.id.toolbar2)
    Toolbar toolbar;
    @Nullable @BindView(R.id.description_list)
    ListView list;

    private static final String TAG = "MainActivity";


    String[] description_items ={
            "Match Name",
            "Location",
            "Date",
            "Time",
            "Status",
            "Created at"
    };
//    String[] description_details ={
//            "I am a match",
//            "HKU",
//            "2017-11-12",
//            "06:00pm - 07:50pm",
//            "2017-11-12 06:22pm"
//    };

    Integer[] description_icons ={
            R.drawable.ic_turned_in_not_black_24dp,
            R.drawable.ic_room_black_24dp,
            R.drawable.ic_event_black_24dp,
            R.drawable.ic_schedule_black_24dp,
            R.drawable.ic_bubble_chart_black_24dp,
            R.drawable.ic_settings_backup_restore_black_24dp
    };

    private Runnable keepScoreUpdate = new Runnable() {
        public void run() {
            Log.d(TAG, "Thread started!");
            while (true) {
                while (session.isScoreReady()) {
                    try {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                jm.db_updateScore(new JSONManager.ServerCallback() {
                                    @Override
                                    public void onSuccess(JSONObject result) {
                                        try {
                                            if (!result.getString("match_id").equals("-1")) {
                                                p1_score.setText(result.getJSONObject("score").getString("p1_score"));
                                                p2_score.setText(result.getJSONObject("score").getString("p2_score"));
                                            } else {
                                                Log.d(TAG, "It's -1!");
                                            }
                                        } catch (JSONException e) {
                                        }
                                    }
                                });
                            }
                        }).start();
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (sh != null) {
            sh.removeCallbacks(keepScoreUpdate);
        }
        if (st != null) {
            st.quit();
        }
        session.ScoreisNotReady();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("MatchHandi");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // SqLite database handler
        c = this;
        db = new SQLiteHandler(c);
        session = new SessionManager(c);
        session.ScoreisNotReady();
        jm = new JSONManager(c);


        btnEditMatch.setVisibility(View.GONE);
        mkd.setVisibility(View.GONE);
        btn_p1_decrease_score.setVisibility(View.GONE);
        btn_p1_increase_score.setVisibility(View.GONE);
        btn_p2_decrease_score.setVisibility(View.GONE);
        btn_p2_increase_score.setVisibility(View.GONE);

        st = new HandlerThread("name");
        st.start();
        sh = new Handler(st.getLooper());
        sh.post(keepScoreUpdate);

        jm.db_getScore(new JSONManager.ServerCallback() {
            @Override
            public void onSuccess(JSONObject result) {

                matchid = session.getCurrentMatch();

                m = db.getMatch().get(matchid);
                s = db.getScore().get(matchid);
                u = db.getUserDetails();
                p1_name.setText(String.valueOf(s.getP1_name()));
                p1_score.setText(String.valueOf(s.getP1_score()));
                p2_name.setText(String.valueOf(s.getP2_name()));
                p2_score.setText(String.valueOf(s.getP2_score()));
                session.ScoreisReady();


                Log.d(TAG, "OwnerID: " + m.getOwnerID() + " UserID:" + u.get("id"));
                if (String.valueOf(m.getOwnerID()).equals(u.get("id")) && getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    mkd.setVisibility(View.VISIBLE);
                    btnEditMatch.setVisibility(View.VISIBLE);
                    btn_p1_decrease_score.setVisibility(View.VISIBLE);
                    btn_p1_increase_score.setVisibility(View.VISIBLE);
                    btn_p2_decrease_score.setVisibility(View.VISIBLE);
                    btn_p2_increase_score.setVisibility(View.VISIBLE);
                    Log.d(TAG, "ID Match!!!");
                }


                Log.d(TAG, "MATCHID " +String.valueOf(matchid));

//        String date = m.getStart_at().replaceAll(" .*", "");
                date = m.getStart_at().substring(0, 10);

                starttime = m.getStart_at().substring(m.getStart_at().length() - 8, m.getStart_at().length() -3);
                endtime = m.getFinish_at().substring(m.getFinish_at().length() - 8, m.getFinish_at().length()-3);

                status = "Scheduled";
                switch(m.getStatus()) {
                    case 1:
                        status = "Started";
                        break;
                    case 2:
                        status = "Finished";
                        break;
                    default:
                }

                String[] description_details = {
                        m.getName(),
                        m.getType(),
                        date,
                        starttime + " - " + endtime,
                        status,
                        m.getCreated_at()
                };


                DescriptionListAdapter adapter=new DescriptionListAdapter((MatchActivity)c, description_items,
                                                                        description_details, description_icons);
                list.setAdapter(adapter);

                // Edit Match click event
                btnEditMatch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent myIntent = new Intent(v.getContext(), EditMatchActivity.class);
                        myIntent.putExtra("Name",m.getName());
                        myIntent.putExtra("Location", m.getType());
                        myIntent.putExtra("Date", date);
                        myIntent.putExtra("StartTime", starttime);
                        myIntent.putExtra("EndTime", endtime);
                        myIntent.putExtra("Status", m.getStatus());
                        myIntent.putExtra("P1Name", String.valueOf(s.getP1_name()));
                        myIntent.putExtra("P2Name", String.valueOf(s.getP2_name()));
                        startActivityForResult(myIntent, 2);
                    }
                });

                // Add Point
                btn_add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int score = Integer.valueOf(marksPer.getText().toString()) + 1;
                        marksPer.setText(String.valueOf(score));
                    }
                });

                // Minus Point
                btn_minus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int score = Integer.valueOf(marksPer.getText().toString());
                        if(score > 0 ) score--;
                        marksPer.setText(String.valueOf(score));
                    }
                });

                // MarksPer on Change
                marksPer.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void afterTextChanged(Editable s) {
                        // TODO Auto-generated method stub
                        if(!TextUtils.isEmpty(marksPer.getText())) {
                            int marksAdj = Integer.valueOf(marksPer.getText().toString());
                            String p;
                            if(marksAdj > 1) {p = " Points";} else {p = " Point";}
                            btn_p1_increase_score.setText("+ " + marksPer.getText().toString() + p);
                            btn_p1_decrease_score.setText("- " + marksPer.getText().toString() + p);
                            btn_p2_increase_score.setText("+ " + marksPer.getText().toString() + p);
                            btn_p2_decrease_score.setText("- " + marksPer.getText().toString() + p);
                        }
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        // TODO Auto-generated method stub
                    }
                });

            }
        });
//        jm.db_getMatch();





        // Player 1 Increase Point

        btn_p1_increase_score.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int score = Integer.valueOf(p1_score.getText().toString()) + Integer.valueOf(marksPer.getText().toString());
                p1_score.setText(String.valueOf(score));
                updateScore();
            }
        });

        // Player 1 Decrease Point

        btn_p1_decrease_score.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int score = Integer.valueOf(p1_score.getText().toString()) - Integer.valueOf(marksPer.getText().toString());
                p1_score.setText(String.valueOf(score));
                updateScore();
            }
        });

        // Player 2 Increase Point

        btn_p2_increase_score.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int score = Integer.valueOf(p2_score.getText().toString()) + Integer.valueOf(marksPer.getText().toString());
                p2_score.setText(String.valueOf(score));
                updateScore();
            }
        });

        // Player 2 Decrease Point

        btn_p2_decrease_score.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int score = Integer.valueOf(p2_score.getText().toString()) - Integer.valueOf(marksPer.getText().toString());
                p2_score.setText(String.valueOf(score));
                updateScore();
            }
        });
    }

    private void DescriptionRefresh() {
        finish();
        startActivity(getIntent());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        // code here to show dialog
        super.onBackPressed();  // optional depending on your needs
    }

    public void updateScore(){
        jm.db_newScore(matchid, s.getP1_name(), Integer.parseInt(p1_score.getText().toString()),
                                s.getP2_name(), Integer.parseInt(p2_score.getText().toString()),
                                new JSONManager.ServerCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                jm.db_getScore(new JSONManager.ServerCallback() {
                    @Override
                    public void onSuccess(JSONObject result) {}
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 2:
                DescriptionRefresh();
        }
    }
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            btnEditMatch.setVisibility(View.GONE);
            mkd.setVisibility(View.GONE);
            btn_p1_decrease_score.setVisibility(View.GONE);
            btn_p1_increase_score.setVisibility(View.GONE);
            btn_p2_decrease_score.setVisibility(View.GONE);
            btn_p2_increase_score.setVisibility(View.GONE);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            if (String.valueOf(m.getOwnerID()).equals(u.get("id"))) {
            mkd.setVisibility(View.VISIBLE);
            btnEditMatch.setVisibility(View.VISIBLE);
            btn_p1_decrease_score.setVisibility(View.VISIBLE);
            btn_p1_increase_score.setVisibility(View.VISIBLE);
            btn_p2_decrease_score.setVisibility(View.VISIBLE);
            btn_p2_increase_score.setVisibility(View.VISIBLE);
        }

        }
    }
}