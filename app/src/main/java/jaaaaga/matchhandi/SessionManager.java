package jaaaaga.matchhandi;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

class SessionManager {
    // Shared preferences file name
    private static final String PREF_NAME = "MatchHandi";
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    private static final String CURRENT_MATCH_ID = "MatchID";
    private static final String CURRENT_SCORE_ID = "ScoreID";
    private static final String CURRENT_USER_ID = "UserID";
    private static final String JSON_IS_READY = "JsonisReady";
    private static final String KEY_IS_SCORE_READY = "ScoreisReady";

    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();
    // Shared Preferences
    private SharedPreferences pref;
    private Editor editor;
    private Context _context;
    // Shared pref mode
    protected int PRIVATE_MODE = 0;

    SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.apply();
    }
    void setCurrentUserId(int userID) {
        editor.putInt(CURRENT_USER_ID, userID);
        editor.commit();
    }
    int getCurrentUserID() {
        return pref.getInt(CURRENT_USER_ID, 0);
    }

    void JsonIsReady() {
        editor.putBoolean(JSON_IS_READY, true);
        editor.commit();
        Log.d(TAG, "Json is set as ready!");
    }

    void ScoreisReady() {
        editor.putBoolean(KEY_IS_SCORE_READY, true);
        editor.commit();
        Log.d(TAG, "Score is ready!");
    }

    void ScoreisNotReady() {
        editor.putBoolean(KEY_IS_SCORE_READY, false);
        editor.commit();
        Log.d(TAG, "Score is not ready!");
    }
    boolean isJsonReady() {
        return (pref.getBoolean(JSON_IS_READY, false));
    }
    void setLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    boolean isScoreReady() {
        return pref.getBoolean(KEY_IS_SCORE_READY, false);
    }
    boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    void setCurrentMatch(int matchid) {
        editor.putInt(CURRENT_MATCH_ID, matchid);

        editor.commit();
    }

    void setCurrentScore(int scoreid) {
        editor.putInt(CURRENT_SCORE_ID, scoreid);

        editor.commit();
    }

    int getCurrentMatch() {
        return pref.getInt(CURRENT_MATCH_ID, 0);
    }

}