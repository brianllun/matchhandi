package jaaaaga.matchhandi;

public class AppConfig {
    // Server user login url
    public static String URL_LOGIN = "http://matchhandi.com/android_api/login.php";

    // Server user register url
    public static String URL_REGISTER = "http://matchhandi.com/android_api/register.php";

    public static String URL_GETMATCH = "http://matchhandi.com/android_api/getmatch.php";

    public static String URL_SAVEMATCH = "http://matchhandi.com/android_api/savematch.php";

    public static String URL_WRITESCORE = "http://matchhandi.com/android_api/writescore.php";

    public static String URL_GETSCORE = "http://matchhandi.com/android_api/getscore.php";

    public static String URL_UPDATESCORE = "http://matchhandi.com/android_api/updatescore.php";
}