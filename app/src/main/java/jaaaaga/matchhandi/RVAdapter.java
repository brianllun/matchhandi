package jaaaaga.matchhandi;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.util.Linkify;
import android.view.*;
import android.widget.Filterable;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;


public class RVAdapter extends RecyclerView.Adapter<RVAdapter.MyViewHolder> implements Filterable{

    private Context mContext;
    public static ArrayList<Match> matchList, filterList;
    private RVAdapterListener mlistener;
    private MatchFilter filter;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            count = (TextView) view.findViewById(R.id.count);
        }
    }


    public RVAdapter(Context mContext, ArrayList<Match> matchList, RVAdapterListener listener) {
        this.mContext = mContext;
        this.matchList = matchList;
        this.mlistener = listener;
        this.filterList = matchList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.match_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Match match = matchList.get(position);
        holder.title.setText(match.getName());
        holder.count.setText("MatchID: " + match.getMatchID());

        holder.title.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                mlistener.onCardSelected(position);
            }
        });
        holder.count.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                mlistener.onCardSelected(position);
            }
        });
    }


    public void updateList(ArrayList<Match> matchList){
        this.matchList = matchList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return this.matchList.size();
    }

    public interface RVAdapterListener {

        void onCardSelected(int position);
    }

    //RETURN FILTER OBJ
    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new MatchFilter(filterList,this);
        }
        return filter;
    }
}