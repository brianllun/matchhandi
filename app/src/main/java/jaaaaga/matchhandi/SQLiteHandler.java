package jaaaaga.matchhandi;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

public class SQLiteHandler extends SQLiteOpenHelper {

    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "matchhandi";

    // Login table name
    private static final String TABLE_USER = "users";
    private static final String TABLE_MATCH = "matches";
    private static final String TABLE_SCORE = "scores";

    // Login Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_FIRSTNAME = "firstname";
    private static final String KEY_LASTNAME = "lastname";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_UID = "uid";
    private static final String KEY_CREATED_AT = "created_at";

    private static final String KEY_SCORE_ID = "score_ID";
    private static final String KEY_p1NAME = "p1_name";
    private static final String KEY_p1SCORE = "p1_score";
    private static final String KEY_p2NAME = "p2_name";
    private static final String KEY_p2SCORE = "p2_score";

    private static final String KEY_MATCH_ID = "match_ID";
    private static final String KEY_OWNER_ID = "owner_ID";
    private static final String KEY_BOARD_ID = "board_ID";
    private static final String KEY_NAME = "name";
    private static final String KEY_TYPE = "type";
    private static final String KEY_STATUS = "status";
    private static final String KEY_START_AT = "start_at";
    private static final String KEY_FINISH_AT = "finish_at";


    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_USERNAME + " TEXT,"
                + KEY_FIRSTNAME + " TEXT," + KEY_LASTNAME + " TEXT,"
                + KEY_EMAIL + " TEXT UNIQUE," + KEY_CREATED_AT + " TEXT" + ")";
        db.execSQL(CREATE_LOGIN_TABLE);
        String CREATE_MATCH_TABLE = "CREATE TABLE " + TABLE_MATCH + "("
                + KEY_MATCH_ID + " INTEGER PRIMARY KEY," + KEY_OWNER_ID + " INTEGER,"
                + KEY_BOARD_ID + " INTEGER," + KEY_NAME + " TEXT,"
                + KEY_TYPE + " TEXT," + KEY_STATUS + " INTEGER,"
                + KEY_CREATED_AT + " TEXT ," + KEY_START_AT + " TEXT,"
                + KEY_FINISH_AT + " TEXT" + ")";
        db.execSQL(CREATE_MATCH_TABLE);
        String CREATE_SCORE_TABLE = "CREATE TABLE " + TABLE_SCORE + "("
                + KEY_SCORE_ID + " INTEGER PRIMARY KEY," + KEY_MATCH_ID + " INTEGER,"
                + KEY_p1NAME + " TEXT," + KEY_p1SCORE + " INTEGER,"
                + KEY_p2NAME + " TEXT," + KEY_p2SCORE + " INTEGER,"
                + KEY_CREATED_AT + " TEXT, UNIQUE (" + KEY_SCORE_ID + ") ON CONFLICT REPLACE)";
        db.execSQL(CREATE_SCORE_TABLE);

        Log.d(TAG, "Database tables created");
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MATCH);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCORE);
        // Create tables again
        onCreate(db);
    }

    //    public void updateScore(String score_ID, String match_ID, String p1_score, String p2_score, String created_at) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(KEY_SCORE_ID, score_ID);
//        values.put(KEY_p1SCORE, p1_score);
//        values.put(KEY_p2SCORE, p2_score);
//        values.put(KEY_CREATED_AT, created_at);
////        db.delete(TABLE_SCORE, "WHERE match_ID = " + match_ID, null);
//        db.update(TABLE_SCORE, values, KEY_MATCH_ID + " = " + match_ID, null);
//    }
    public void addMatch(String match_ID, String owner_ID, String board_ID,
                         String name, String type, String status,
                         String created_at, String start_at, String finish_at) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_MATCH_ID, match_ID);
        values.put(KEY_OWNER_ID, owner_ID);
        values.put(KEY_BOARD_ID, board_ID);
        values.put(KEY_NAME, name);
        values.put(KEY_TYPE, type);
        values.put(KEY_STATUS, status);
        values.put(KEY_CREATED_AT, created_at);
        values.put(KEY_START_AT, start_at);
        values.put(KEY_FINISH_AT, finish_at);

        long id = db.insert(TABLE_MATCH, null, values);
//        db.close();

    }

    public void addScore(String score_ID, String match_ID, String p1_name,
                         String p1_score, String p2_name, String p2_score,
                         String created_at) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_SCORE_ID, score_ID);
        values.put(KEY_MATCH_ID, match_ID);
        values.put(KEY_p1NAME, p1_name);
        values.put(KEY_p1SCORE, p1_score);
        values.put(KEY_p2NAME, p2_name);
        values.put(KEY_p2SCORE, p2_score);
        values.put(KEY_CREATED_AT, created_at);

        long id = db.insert(TABLE_SCORE, null, values);
//        db.close();

        Log.d(TAG, "New score inserted: " + score_ID);


    }
    /**
     * Storing user details in database
     */
    public void addUser(String user_id, String username, String firstname, String lastname, String email, String created_at) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, user_id);
        values.put(KEY_USERNAME, username); // Name
        values.put(KEY_FIRSTNAME, firstname);
        values.put(KEY_LASTNAME, lastname);
        values.put(KEY_EMAIL, email); // Email
        values.put(KEY_CREATED_AT, created_at); // Created At

        // Inserting Row
        long id = db.insert(TABLE_USER, null, values);
//        db.close(); // Closing database connection

        Log.d(TAG, "New user inserted into sqlite: " + id);
    }

    /**
     * Getting user data from database
     */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("id", cursor.getString(0));
            user.put("username", cursor.getString(1));
            user.put("firstname", cursor.getString(2));
            user.put("lastname", cursor.getString(3));
            user.put("email", cursor.getString(4));
            user.put("created_at", cursor.getString(5));
        }
        cursor.close();
//        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

        return user;
    }

    public HashMap<Integer, Match> getMatch() {
        Map<Integer, Match> match = new HashMap<Integer, Match>();
        String selectQuery = "SELECT  * FROM " + TABLE_MATCH;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            while (!cursor.isAfterLast()) {
                Match m = new Match(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2),
                        cursor.getString(3), cursor.getString(4), cursor.getInt(5),
                        cursor.getString(6), cursor.getString(7), cursor.getString(8));
                match.put(cursor.getInt(0),m);
                cursor.moveToNext();
            }
        }
        cursor.close();
//        db.close();
        // return user
        Log.d(TAG, "Fetching match from Sqlite: " + match.toString());

        return (HashMap)match;
    }

    public HashMap<Integer, Score> getScore() {
        Map<Integer, Score> score = new HashMap<Integer, Score>();
        String selectQuery = "SELECT  * FROM " + TABLE_SCORE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            while (!cursor.isAfterLast()) {
                Score s = new Score(cursor.getInt(0), cursor.getInt(1), cursor.getString(2),
                        cursor.getInt(3), cursor.getString(4), cursor.getInt(5),
                        cursor.getString(6));
                score.put(cursor.getInt(1),s);
                cursor.moveToNext();
            }
        }
        cursor.close();
//        db.close();
        // return user
        Log.d(TAG, "Fetching score from Sqlite: " + score.toString());

        return (HashMap)score;
    }

    /**
     * Re crate database Delete all tables and create them again
     */
    public void deleteScores() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_SCORE, null, null);
//        db.close();

        Log.d(TAG, "Deleted all scores from sqlite");
    }
    public void deleteMatches() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_MATCH, null, null);
//        db.close();

        Log.d(TAG, "Deleted all matches info from sqlite");
    }
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_USER, null, null);
//        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }
    public void resetMatches() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MATCH);
        String CREATE_MATCH_TABLE = "CREATE TABLE " + TABLE_MATCH + "("
                + KEY_MATCH_ID + " INTEGER PRIMARY KEY," + KEY_OWNER_ID + " INTEGER,"
                + KEY_BOARD_ID + " INTEGER," + KEY_NAME + " TEXT,"
                + KEY_TYPE + " TEXT," + KEY_STATUS + " INTEGER,"
                + KEY_CREATED_AT + " TEXT ," + KEY_START_AT + " TEXT,"
                + KEY_FINISH_AT + " TEXT" + ")";
        db.execSQL(CREATE_MATCH_TABLE);
////        db.close();
    }
    public void resetScores() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCORE);
        String CREATE_SCORE_TABLE = "CREATE TABLE " + TABLE_SCORE + "("
                + KEY_SCORE_ID + " INTEGER PRIMARY KEY," + KEY_MATCH_ID + " INTEGER,"
                + KEY_p1NAME + " TEXT," + KEY_p1SCORE + " INTEGER,"
                + KEY_p2NAME + " TEXT," + KEY_p2SCORE + " INTEGER,"
                + KEY_CREATED_AT + " TEXT" + ")";
        db.execSQL(CREATE_SCORE_TABLE);
//        db.close();
    }

}