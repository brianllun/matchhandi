package jaaaaga.matchhandi;

import android.os.Parcel;
import android.os.Parcelable;


public class Score  implements Parcelable{
    private int scoreID;
    private int matchID;
    private String p1_name;
    private String p2_name;
    private int p1_score;
    private int p2_score;
    private String created_at;
    private int resourceID;

    Score(int scoreID, int matchID, String p1_name, int p1_score, String p2_name,
          int p2_score, String created_at) {
        this.scoreID = scoreID;
        this.matchID = matchID;
        this.p1_name = p1_name;
        this.p2_name = p2_name;
        this.p1_score = p1_score;
        this.p2_score = p2_score;
        this.created_at = created_at;
    }
    Score() {}

//    Match(int matchID, String name, String type, int status, String start_at, String finish_at) {
//        this.matchID = matchID;
//        this.ownerID = 2;
//        this.boardID = 1;
//        this.name = name;
//        this.type = type;
//        this.status = status;
//        this.start_at = start_at;
//        this.finish_at = finish_at;
//    }

    Score(int scoreID) {
        this.scoreID = scoreID;
    }


    public int getScoreID() {
        return (this.scoreID);
    }
    public int getResourceID() {
        return (this.resourceID);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(scoreID);
        dest.writeInt(resourceID);
    }

    public static final Parcelable.Creator<Score> CREATOR = new Creator<Score>() {
        public Score createFromParcel(Parcel source) {
            Score mScore = new Score();
            mScore.scoreID = source.readInt();
            mScore.resourceID = source.readInt();
            return mScore;
        }
        public Score[] newArray(int size) {
            return new Score[size];
        }
    };

    public int getMatchID() {
        return matchID;
    }

    public void setMatchID(int matchID) {
        this.matchID = matchID;
    }

    public String getP1_name() {
        return p1_name;
    }

    public void setP1_name (String p1_name) {
        this.p1_name = p1_name;
    }

    public int getP1_score() {
        return p1_score;
    }

    public void setP1_score(int p1_score) {
        this.p1_score = p1_score;
    }

    public String getP2_name() {
        return p2_name;
    }

    public void setP2_name (String p2_name) {
        this.p2_name = p2_name;
    }

    public int getP2_score() {
        return p2_score;
    }

    public void setP2_score(int p2_score) {
        this.p2_score = p2_score;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


}

